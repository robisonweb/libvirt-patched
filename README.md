# libvirt-patched

This is the same libvirt package provided by Arch Linux's Arch Build System,
but with cherry-picked patches I find useful.

## Current Patches

Currently there is only one patch:

* Enable the fallback URI of `qemu:///system` as a convenience.
