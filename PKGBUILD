# Maintainer: Joe Robison <joe@robison.dev>
# Maintainer: Sven-Hendrik Haase <svenstaro@archlinux.org>
# Maintainer: Robin Broda <robin@broda.me>
# Contributor: Christian Rebischke <chris.rebischke@archlinux.org>
# Contributor: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Jonathan Wiersma <archaur at jonw dot org>

# Changes from official Arch build:
# - Use $pkgrel .incremental for our fixes
# - Add $epoch to depends lines because otherwise you can't install
#   the optional modules (gluster, iscsi, rbd). Also do this on provides
#   for consistency.
# - Patch to use qemu://system/ as default uri per Debian/Ubuntu

pkgbase=libvirt
pkgname=(libvirt-patched libvirt-storage-gluster-patched libvirt-storage-iscsi-direct-patched libvirt-storage-rbd-patched)
epoch=1
pkgver=8.0.0
pkgrel=2.2
pkgdesc="API for controlling virtualization engines (openvz,kvm,qemu,virtualbox,xen,etc). With light convenience patches."
arch=('x86_64')
url="https://libvirt.org/"
license=('LGPL' 'GPL3') #libvirt_parthelper links to libparted which is GPL3 only
depends=('libpciaccess' 'yajl' 'fuse2' 'gnutls' 'parted' 'libssh' 'libxml2' 'numactl' 'polkit')
makedepends=('meson' 'libxslt' 'python-docutils' 'lvm2' 'open-iscsi' 'libiscsi' 'ceph-libs' 'glusterfs'
             'bash-completion' 'rpcsvc-proto' 'dnsmasq' 'iproute2' 'qemu-headless')
optdepends=('libvirt-storage-gluster-patched: Gluster storage backend'
            'libvirt-storage-iscsi-direct-patched: iSCSI-direct storage backend'
            'libvirt-storage-rbd-patched: RBD storage backend'
            'gettext: required for libvirt-guests.service'
            'openbsd-netcat: for remote management over ssh'
            'dmidecode: DMI system info support'
            'dnsmasq: required for default NAT/DHCP for guests'
            'radvd: IPv6 RAD support'
            'ebtables: required for default NAT networking'
            'qemu: QEMU/KVM support'
            'lvm2: Logical Volume Manager support'
            'open-iscsi: iSCSI support via iscsiadm')

backup=(
  'etc/conf.d/libvirtd'
  'etc/conf.d/libvirt-guests'
  'etc/conf.d/virtchd'
  'etc/conf.d/virtinterfaced'
  'etc/conf.d/virtlockd'
  'etc/conf.d/virtlogd'
  'etc/conf.d/virtlxcd'
  'etc/conf.d/virtnetworkd'
  'etc/conf.d/virtnodedevd'
  'etc/conf.d/virtnwfilterd'
  'etc/conf.d/virtproxyd'
  'etc/conf.d/virtqemud'
  'etc/conf.d/virtsecretd'
  'etc/conf.d/virtstoraged'
  'etc/conf.d/virtvboxd'
  'etc/libvirt/libvirt-admin.conf'
  'etc/libvirt/libvirt.conf'
  'etc/libvirt/libvirtd.conf'
  'etc/libvirt/lxc.conf'
  'etc/libvirt/nwfilter/allow-arp.xml'
  'etc/libvirt/nwfilter/allow-dhcp-server.xml'
  'etc/libvirt/nwfilter/allow-dhcpv6-server.xml'
  'etc/libvirt/nwfilter/allow-dhcp.xml'
  'etc/libvirt/nwfilter/allow-dhcpv6.xml'
  'etc/libvirt/nwfilter/allow-incoming-ipv4.xml'
  'etc/libvirt/nwfilter/allow-incoming-ipv6.xml'
  'etc/libvirt/nwfilter/allow-ipv6.xml'
  'etc/libvirt/nwfilter/allow-ipv4.xml'
  'etc/libvirt/nwfilter/clean-traffic-gateway.xml'
  'etc/libvirt/nwfilter/clean-traffic.xml'
  'etc/libvirt/nwfilter/no-arp-ip-spoofing.xml'
  'etc/libvirt/nwfilter/no-arp-mac-spoofing.xml'
  'etc/libvirt/nwfilter/no-arp-spoofing.xml'
  'etc/libvirt/nwfilter/no-ip-multicast.xml'
  'etc/libvirt/nwfilter/no-ipv6-multicast.xml'
  'etc/libvirt/nwfilter/no-ip-spoofing.xml'
  'etc/libvirt/nwfilter/no-ipv6-spoofing.xml'
  'etc/libvirt/nwfilter/no-mac-spoofing.xml'
  'etc/libvirt/nwfilter/no-mac-broadcast.xml'
  'etc/libvirt/nwfilter/no-other-l2-traffic.xml'
  'etc/libvirt/nwfilter/no-other-rarp-traffic.xml'
  'etc/libvirt/nwfilter/qemu-announce-self-rarp.xml'
  'etc/libvirt/nwfilter/qemu-announce-self.xml'
  'etc/libvirt/qemu.conf'
  'etc/libvirt/qemu-lockd.conf'
  'etc/libvirt/qemu/networks/default.xml'
  'etc/libvirt/virtchd.conf'
  'etc/libvirt/virtinterfaced.conf'
  'etc/libvirt/virtlockd.conf'
  'etc/libvirt/virtlogd.conf'
  'etc/libvirt/virt-login-shell.conf'
  'etc/libvirt/virtlxcd.conf'
  'etc/libvirt/virtnetworkd.conf'
  'etc/libvirt/virtnodedevd.conf'
  'etc/libvirt/virtnwfilterd.conf'
  'etc/libvirt/virtproxyd.conf'
  'etc/libvirt/virtqemud.conf'
  'etc/libvirt/virtsecretd.conf'
  'etc/libvirt/virtstoraged.conf'
  'etc/libvirt/virtvboxd.conf'
  'etc/logrotate.d/libvirtd'
  'etc/logrotate.d/libvirtd.lxc'
  'etc/logrotate.d/libvirtd.qemu'
  'etc/sasl2/libvirt.conf'
)
source=("https://libvirt.org/sources/$pkgbase-$pkgver.tar.xz"{,.asc}
        "$pkgbase"-fix-snapshot-revert.patch::https://gitlab.com/libvirt/libvirt/-/commit/76deb65.patch
        "$pkgbase"-fix-virProcessGetStatInfo.patch::https://gitlab.com/libvirt/libvirt/-/commit/105dace.patch
	"add-fallback-uri.patch")
sha256sums=('51e6e8ff04bafe96d7e314b213dcd41fb1163d9b4f0f75cdab01e663728f4cf6'
            'SKIP'
            '06164af980ab2ece221de494f4da5a0d38c22c0f1ac527dd53aed2e2dbf1ed48'
            'a7b0f7b2941c0fed41205c8db3d3d37ed9ccee910e44196d2d2bf5aa0eb986d8'
            '07bc4ddc8a6bccebaa143456d1722315175add412e45994057ae67a6b7863980')
validpgpkeys=('453B65310595562855471199CA68BE8010084C9C') # Jiří Denemark <jdenemar@redhat.com>

prepare() {
  cd "$pkgbase-$pkgver"

  sed -i 's|/sysconfig/|/conf.d/|g' \
    src/remote/libvirtd.service.in \
    tools/{libvirt-guests.service,libvirt-guests.sh,virt-pki-validate}.in \
    src/locking/virtlockd.service.in \
    src/logging/virtlogd.service.in
  sed -i 's|/usr/libexec/qemu-bridge-helper|/usr/lib/qemu/qemu-bridge-helper|g' \
    src/qemu/qemu.conf \
    src/qemu/test_libvirtd_qemu.aug.in

  # bugs fixed post-release
  patch -Np1 < ../libvirt-fix-snapshot-revert.patch
  patch -Np1 < ../libvirt-fix-virProcessGetStatInfo.patch

  # My single patch
  patch -p1 -i ${srcdir}/add-fallback-uri.patch
}

build() {
  cd "$pkgbase-$pkgver"

  arch-meson build \
    --libexecdir=lib/libvirt \
    -Drunstatedir=/run \
    -Dqemu_group=kvm \
    -Dnetcf=disabled \
    -Dopenwsman=disabled \
    -Dapparmor=disabled \
    -Dapparmor_profiles=disabled \
    -Dselinux=disabled \
    -Dwireshark_dissector=disabled \
    -Ddriver_bhyve=disabled \
    -Ddriver_hyperv=disabled \
    -Ddriver_libxl=disabled \
    -Ddriver_vz=disabled \
    -Dsanlock=disabled \
    -Dsecdriver_apparmor=disabled \
    -Dsecdriver_selinux=disabled \
    -Dstorage_sheepdog=disabled \
    -Dstorage_vstorage=disabled \
    -Ddtrace=disabled \
    -Dnumad=disabled \
    -Dstorage_zfs=enabled \
    -Dstorage_rbd=enabled

  ninja -C build
}

check() {
  cd "$pkgbase-$pkgver"

  ninja -C build test
}

package_libvirt-patched() {
  provides=("libvirt=$pkgver" 'libvirt.so' 'libvirt-admin.so' 'libvirt-lxc.so' 'libvirt-qemu.so')
  conflicts=('libvirt')
  cd "$pkgbase-$pkgver"
  DESTDIR="$pkgdir" ninja -C build install

  mv "$pkgdir"/etc/{sysconfig,conf.d}
  mkdir "$pkgdir"/usr/lib/{sysusers,tmpfiles}.d
  echo "g libvirt - -" > "$pkgdir/usr/lib/sysusers.d/libvirt.conf"
  echo "z /var/lib/libvirt/qemu 0751" > "$pkgdir/usr/lib/tmpfiles.d/libvirt.conf"

  chown 0:102 "$pkgdir/usr/share/polkit-1/rules.d"
  chmod 0750 "$pkgdir/usr/share/polkit-1/rules.d"
  chmod 600 "$pkgdir"/etc/libvirt/nwfilter/*.xml \
    "$pkgdir/etc/libvirt/qemu/networks/default.xml"

  rm -rf \
    "$pkgdir/run" \
    "$pkgdir/var/lib/libvirt/qemu" \
    "$pkgdir/var/cache/libvirt/qemu" \
    "$pkgdir/etc/logrotate.d/libvirtd.libxl"

  rm -f "$pkgdir/etc/libvirt/qemu/networks/autostart/default.xml"

  # move split modules
  mv "$pkgdir"/usr/lib/libvirt/storage-backend/libvirt_storage_backend_{rbd,gluster}.so "$pkgdir/../"
  mv "$pkgdir/usr/lib/libvirt/storage-backend/libvirt_storage_backend_iscsi-direct.so" "$pkgdir/../"
  mv "$pkgdir/usr/lib/libvirt/storage-file/libvirt_storage_file_gluster.so" "$pkgdir/../"
}

package_libvirt-storage-gluster-patched() {
  pkgdesc="Libvirt Gluster storage backend"
  depends=("libvirt-patched=$epoch:$pkgver" 'glusterfs')
  provides=("libvirt-storage-gluster=$epoch:$pkgver")
  conflicts=('libvirt-storage-gluster')
  optdepends=()
  backup=()

  install -Dv -t "$pkgdir/usr/lib/libvirt/storage-backend" "$pkgdir/../libvirt_storage_backend_gluster.so"
  install -Dv -t "$pkgdir/usr/lib/libvirt/storage-file" "$pkgdir/../libvirt_storage_file_gluster.so"
}

package_libvirt-storage-iscsi-direct-patched() {
  pkgdesc="Libvirt iSCSI-direct storage backend"
  depends=("libvirt-patched=$epoch:$pkgver" 'libiscsi')
  provides=("libvirt-storage-iscsi-direct=$epoch:$pkgver")
  conflicts=('libvirt-storage-iscsi-direct')
  optdepends=()
  backup=()

  install -Dv -t "$pkgdir/usr/lib/libvirt/storage-backend" "$pkgdir/../libvirt_storage_backend_iscsi-direct.so"
}

package_libvirt-storage-rbd-patched() {
  pkgdesc="Libvirt RBD storage backend"
  depends=("libvirt-patched=$epoch:$pkgver" 'ceph-libs')
  provides=("libvirt-storage-rbd=$epoch:$pkgver")
  conflicts=('libvirt-storage-rbd')
  optdepends=()
  backup=()

  install -Dv -t "$pkgdir/usr/lib/libvirt/storage-backend" "$pkgdir/../libvirt_storage_backend_rbd.so"
}
